function lili() {
    let liliOut = document.getElementById("lili-out");
    liliOut.value = "";//clears the prevoius data before adding new
    let liWords = ["l", "I"];
    let charCount = function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    };

    for (let int = 0; int < charCount(10, 15); int++) {
        let random_boolean = Math.round(Math.random());
        liliOut.value += liWords[random_boolean];
    }
}


function copyToClipboard() {
    /* Get the text field */
    let copyText = document.getElementById("lili-out");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    //alert("Copied the text: " + copyText.value);

    /* Get the text field */
    let copyFeedback = document.getElementById("copy-feedback");
    copyFeedback.innerHTML = "copied";


    setTimeout(function(){
        copyFeedback.innerHTML = "";
    }, 1000);


}